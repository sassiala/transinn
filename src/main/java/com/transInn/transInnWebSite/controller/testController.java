package com.transInn.transInnWebSite.controller;

import com.transInn.transInnWebSite.services.testService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class testController {
    @Autowired
    private testService _testService;
    @GetMapping("/print")
    public String printName(String _name)
    {
        return _testService.GetMyName("ala");
    }
}

package com.transInn.transInnWebSite.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "devis")
public class devis {
    @Id
    @GeneratedValue
    private int id;
    private String monnaie;
    private String monnaieUnitaire;
    private String sigle;
    private String sigleUnitaire;
}

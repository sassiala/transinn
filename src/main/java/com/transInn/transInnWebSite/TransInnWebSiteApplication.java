package com.transInn.transInnWebSite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransInnWebSiteApplication {

	public static void main(String[] args) {

		SpringApplication.run(TransInnWebSiteApplication.class, args);
	}

}
